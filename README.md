# README #

this repostiory stores some stuff might or might not be useful to everyone else

# tableGenApp #

this is a very rough version for generating phantom boilerplate code, including table scheme and basic CRUD operations. 

to run it, you can use anyway to run a scala main class. you just need to initialize a GenArtifact object and then call the method. after running it, a new file: *table.scala* will be created on the root folder. that file has everything you need. 
here is a very ugly video demo how it works: http://www.screencast.com/t/fiqz11AhYRP

## Limitation ##

* it generates one database one table; if you want one database has more table, you need to run it a few times and manually consolidate the code. 

* it generates update method always using setTo, if you want optional like setIfAssigned, you have to manually change the code 

* it assumes your table has a column "id" that is always the primary key; therefore, it does not support composite primary key

* it always generates 6 methods for CRUD, even you won't use them all 

* it now only supports: String, Int, Long, UUID, and OffsetDateTime; more will require template update 

* the template code is very rigid for now, not easy to change, but [it's something](http://i1.kym-cdn.com/photos/images/facebook/000/114/139/tumblr_lgedv2Vtt21qf4x93o1_40020110725-22047-38imqt.jpg). 

## TODO ##

I have intention to: 

* generate test code 

* support multiple table in one database 

* generate repository code, however this requires a spec definition 

* generate in-memory database